package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.time.Duration;

public class MainActivity extends AppCompatActivity {

    TextView textView ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(getApplicationContext(), "Hello World", Toast.LENGTH_LONG).show();

        Toast toast = new Toast(getApplicationContext());
        textView = new TextView(MainActivity.this);
        textView.setTextColor(Color.RED);
        textView.setTextSize(30);
        textView.setBackgroundColor(Color.GREEN);
        textView.setText("Hello SMD");
        toast.setView(textView);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();





    }
}